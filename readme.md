# Configuração do Ambiente - Laravel PHP - Backend

Após o clone, rodar os seguintes comandos, usando um terminal com BASH

+ composer update (ou composer install)
+ cp .env.example .env e modificar os endereços de acordo com o que está instalando (ex.: Servidor localhost para 10.83.1.21)
+ php artisan key:generate
+ php artisan migrate
+ php artisan passport:key
+ php artisan passport:install
+ chmod -R 777 <caminho da aplicação>

# Ubuntu

+ sudo chown www-data:www-data storage/oauth-*.key
+ sudo chmod 600 storage/oauth-*.key

# CentOS
+ sudo chown apache:apache storage/oauth-*.key
+ sudo chmod 600 storage/oauth-*.key

# Se houver algum problema com a permissão do storage/laravel.log
+ sudo setenforce 0

# Renovação do LetsEncrypt
+ sudo certbot --authenticator standalone --installer apache -d <domain> --pre-hook "service apache2 stop" --post-hook "service apache2 start"
