<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['cors', 'auth:api']], function () {
    Route::resource('areas', 'Api\AreasController');
    Route::resource('comments', 'Api\CommentsController');
    Route::resource('companies', 'Api\CompaniesController');
    Route::resource('logs', 'Api\LogsController');
    Route::resource('medias', 'Api\MediasController');
    Route::resource('menus', 'Api\MenusController');
    Route::resource('parameters', 'Api\ParametersController');
    Route::resource('permissions', 'Api\PermissionsController');
    Route::resource('profiles', 'Api\ProfilesController');
    Route::resource('projects', 'Api\ProjectsController');
    Route::resource('rules', 'Api\RulesController');
    Route::resource('services', 'Api\ServicesController');
    Route::resource('services-types', 'Api\ServicesTypesController');
    Route::resource('skills', 'Api\SkillsController');
    Route::resource('users', 'Api\UsersController');
    Route::post('users/uploads', 'Api\UsersController@uploads');
    Route::resource('users-profiles', 'Api\UsersProfilesController');
    Route::resource('works', 'Api\WorksController');
});