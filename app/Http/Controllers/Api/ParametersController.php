<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ParametersRequest;
use App\Services\ParametersService;

class ParametersController extends Controller
{
    protected $service;

    public function __construct(ParametersService $service)
    {
        $this->service = $service;
    }

    public function index(ParametersRequest $request)
    {

    }
}
