<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\PermissionsRequest;
use App\Services\PermissionsService;

class PermissionsController extends Controller
{
    protected $service;

    public function __construct(PermissionsService $service)
    {
        $this->service = $service;
    }

    public function index(PermissionsRequest $request)
    {

    }
}
