<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CommentsRequest;
use App\Services\CommentsService;

class CommentsController extends Controller
{
    protected $service;

    public function __construct(CommentsService $service)
    {
        $this->service = $service;
    }

    public function index(CommentsRequest $request)
    {

    }
}
