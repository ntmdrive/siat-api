<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ServicesTypesRequest;
use App\Services\ServicesTypesService;
use Illuminate\Http\Request;

class ServicesTypesController extends Controller
{
    protected $service;

    public function __construct(ServicesTypesService $service)
    {
        $this->service = $service;
    }

    public function index(ServicesTypesRequest $request)
    {

    }
}
