<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CompaniesRequest;
use App\Services\CompaniesService;

class CompaniesController extends Controller
{
    protected $service;

    public function __construct(CompaniesService $service)
    {
        $this->service = $service;
    }

    public function index(CompaniesRequest $request)
    {

    }
}
