<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\MediasRequest;
use App\Services\MediasService;

class MediasController extends Controller
{
    public function __construct(MediasService $service)
    {
        $this->service = $service;
    }

    public function index(MediasRequest $request)
    {

    }
}
