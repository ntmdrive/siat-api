<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ServicesRequest;
use App\Services\ServicesService;

class ServicesController extends Controller
{
    protected $service;

    public function __construct(ServicesService $service)
    {
        $this->service = $service;
    }

    public function index(ServicesRequest $request)
    {
        
    }
}
