<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\LogsRequest;
use App\Services\LogsService;

class LogsController extends Controller
{
    protected $service;

    public function __construct(LogsService $service)
    {
        $this->service = $service;
    }

    public function index(LogsRequest $request)
    {

    }
}
