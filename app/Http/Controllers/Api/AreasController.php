<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\AreasRequest;
use App\Services\AreasService;

class AreasController extends Controller
{
    protected $service;

    public function __construct(AreasService $service)
    {
        $this->service = $service;
    }

    public function index(AreasRequest $request)
    {

    }
}
