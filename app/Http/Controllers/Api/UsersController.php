<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\UsersRequest as Request;
use App\Services\UsersService;

class UsersController extends Controller
{
    protected $service;

    public function __construct(UsersService $service)
    {
        $this->service = $service;
    }

    public function index(Request $request)
    {

    }

    public function store(Request $request)
    {
        dd($request);
    }

    public function update(Request $request, $id)
    {
        dd($request);
    }

    public function uploads(Request $request)
    {
        $arquivos = $request->files;

        foreach ($arquivos as $archive)
        {
            echo $archive->getClientOriginalName();
        }
    }
}
