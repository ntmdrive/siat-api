<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\RulesRequest;
use App\Services\RulesService;

class RulesController extends Controller
{
    protected $service;

    public function __construct(RulesService $service)
    {
        $this->service = $service;
    }

    public function index(RulesRequest $request)
    {

    }
}
