<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\WorksRequest;
use App\Services\WorksService;

class WorksController extends Controller
{
    protected $service;

    public function __construct(WorksService $service)
    {
        $this->service = $service;
    }

    public function index(WorksRequest $request)
    {

    }
}
