<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProfilesRequest;
use App\Services\ProfilesService;

class ProfilesController extends Controller
{
    protected $service;

    public function __construct(ProfilesService $service)
    {
        $this->service = $service;
    }

    public function index(ProfilesRequest $request)
    {

    }
}
