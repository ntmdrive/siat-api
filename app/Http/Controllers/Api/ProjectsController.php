<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProjectsRequest;
use App\Services\ProjectsService;

class ProjectsController extends Controller
{
    protected $service;

    public function __construct(ProjectsService $service)
    {
        $this->service = $service;
    }

    public function index(ProjectsRequest $request)
    {

    }
}
