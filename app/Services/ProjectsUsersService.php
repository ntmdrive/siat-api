<?php

namespace App\Services;

use App\Http\Requests\ProjectsUsersRequest;
use App\Repositories\ProjectsUsersRepository;

class ProjectsUsersService extends Service
{
    public function __construct(ProjectsUsersRequest $request, ProjectsUsersRepository $repository)
    {
        $this->request = $request;
        $this->repository = $repository;
    }
}