<?php

namespace App\Services;

use App\Http\Requests\PermissionsRequest;
use App\Repositories\PermissionsRepository;

class PermissionsService extends Service
{
    public function __construct(PermissionsRequest $request, PermissionsRepository $repository)
    {
        $this->request = $request;
        $this->repository = $repository;
    }
}