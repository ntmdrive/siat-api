<?php

namespace App\Services;

use App\Http\Requests\CommentsRequest;
use App\Repositories\CommentsRepository;

class CommentsService extends Service
{
    public function __construct(CommentsRequest $request, CommentsRepository $repository)
    {
        $this->request = $request;
        $this->repository = $repository;
    }
}