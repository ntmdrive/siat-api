<?php

namespace App\Services;

use App\Http\Requests\RulesRequest;
use App\Repositories\RulesRepository;

class RulesService extends Service
{
    public function __construct(RulesRequest $request, RulesRepository $repository)
    {
        $this->request = $request;
        $this->repository = $repository;
    }
}