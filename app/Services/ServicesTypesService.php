<?php

namespace App\Services;

use App\Http\Requests\ServicesTypesRequest;
use App\Repositories\ServicesTypesRepository;

class ServicesTypesService extends Service
{
    public function __construct(ServicesTypesRequest $request, ServicesTypesRepository $repository)
    {
        $this->request = $request;
        $this->repository = $repository;
    }
}