<?php

namespace App\Services;

use App\Http\Requests\UsersRequest;
use App\Repositories\UsersRepository;

class UsersService extends Service
{
    public function __construct(UsersRequest $request, UsersRepository $repository)
    {
        $this->request = $request;
        $this->repository = $repository;
    }
}