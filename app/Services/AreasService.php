<?php

namespace App\Services;

use App\Http\Requests\AreasRequest;
use App\Repositories\AreasRepository;

class AreasService extends Service
{
    public function __construct(AreasRequest $request, AreasRepository $repository)
    {
        $this->request = $request;
        $this->repository = $repository;
    }
}