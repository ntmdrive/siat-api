<?php

namespace App\Services;

use App\Http\Requests\LogsRequest;
use App\Repositories\LogsRepository;

class LogsService extends Service
{
    public function __construct(LogsRequest $request, LogsRepository $repository)
    {
        $this->request = $request;
        $this->repository = $repository;
    }
}