<?php

namespace App\Services;

use App\Http\Requests\MediasRequest;
use App\Repositories\MediasRepository;

class MediasService extends Service
{
    public function __construct (MediasRequest $request, MediasRepository $repository)
    {
        $this->request = $request;
        $this->repository = $repository;
    }
}