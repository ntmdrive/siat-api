<?php

namespace App\Services;

use App\Http\Requests\WorksRequest;
use App\Repositories\WorksRepository;

class WorksService extends Service
{
    public function __construct(WorksRequest $request, WorksRepository $repository)
    {
        $this->request = $request;
        $this->repository = $repository;
    }
}