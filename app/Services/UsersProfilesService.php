<?php

namespace App\Services;

use App\Http\Requests\UsersProfilesRequest;
use App\Repositories\UsersProfilesRepository;

class UsersProfilesService extends Service
{
    public function __construct(UsersProfilesRequest $request, UsersProfilesRepository $repository)
    {
        $this->request = $request;
        $this->repository = $repository;
    }
}