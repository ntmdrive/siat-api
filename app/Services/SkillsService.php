<?php

namespace App\Services;

use App\Http\Requests\SkillsRequest;
use App\Repositories\SkillsRepository;

class SkillsService extends Service
{
    public function __construct(SkillsRequest $request, SkillsRepository $repository)
    {
        $this->request = $request;
        $this->repository = $repository;
    }
}