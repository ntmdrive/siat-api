<?php

namespace App\Services;

use App\Http\Requests\MenusRequest;
use App\Repositories\MenusRepository;

class MenusService extends Service
{
    public function __construct(MenusRequest $request, MenusRepository $repository)
    {
        $this->request = $request;
        $this->repository = $repository;
    }
}