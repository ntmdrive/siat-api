<?php

namespace App\Services;

use App\Http\Requests\ServicesUsersRequest;
use App\Repositories\ServicesUsersRepository;

class ServicesUsersService extends Service
{
    public function __construct(ServicesUsersRequest $request, ServicesUsersRepository $repository)
    {
        $this->request = $request;
        $this->repository = $repository;
    }
}