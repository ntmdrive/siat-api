<?php

namespace App\Services;

use App\Http\Requests\ServicesRequest;
use App\Repositories\ServicesRepository;

class ServicesService extends Service
{
    public function __construct(ServicesRequest $request, ServicesRepository $repository)
    {
        $this->request = $request;
        $this->repository = $repository;
    }
}