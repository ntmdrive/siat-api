<?php

namespace App\Services;

use App\Http\Requests\ProjectsRequest;
use App\Repositories\ProjectsRepository;

class ProjectsService extends Service
{
    public function __construct(ProjectsRequest $request, ProjectsRepository $repository)
    {
        $this->request = $request;
        $this->repository = $repository;
    }
}