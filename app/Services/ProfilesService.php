<?php

namespace App\Services;

use App\Http\Requests\ProfilesRequest;
use App\Repositories\ProfilesRepository;

class ProfilesService extends Service
{
    public function __construct (ProfilesRequest $request, ProfilesRepository $repository)
    {
        $this->request = $request;
        $this->repository = $repository;
    }
}