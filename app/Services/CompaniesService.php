<?php

namespace App\Services;

use App\Http\Requests\CompaniesRequest;
use App\Repositories\CompaniesRepository;

class CompaniesService extends Service
{
    public function __construct(CompaniesRequest $request, CompaniesRepository $repository)
    {
        $this->request = $request;
        $this->repository = $repository;
    }
}