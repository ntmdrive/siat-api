<?php

namespace App\Services;

use App\Http\Requests\ParametersRequest;
use App\Repositories\ParametersRepository;

class ParametersService extends Service
{
    public function __construct(ParametersRequest $request, ParametersRepository $repository)
    {
        $this->request = $request;
        $this->repository = $repository;
    }
}