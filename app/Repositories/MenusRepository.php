<?php

namespace App\Repositories;

use App\Models\Menus;

class MenusRepository
{
    protected $model;

    public function __construct(Menus $model)
    {
        $this->model = $model;
    }
}