<?php

namespace App\Repositories;

use App\Models\ServicesUsers;

class ServicesUsersRepository
{
    protected $model;

    public function __construct(ServicesUsers $model)
    {
        $this->model = $model;
    }
}