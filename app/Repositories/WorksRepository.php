<?php

namespace App\Repositories;

use App\Models\Works;

class WorksRepository
{
    protected $model;

    public function __construct(Works $model)
    {
        $this->model = $model;
    }
}