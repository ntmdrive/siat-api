<?php

namespace App\Repositories;

use App\Models\Parameters;

class ParametersRepository
{
    protected $model;

    public function __construct(Parameters $model)
    {
        $this->model = $model;
    }
}