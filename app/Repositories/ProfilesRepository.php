<?php

namespace App\Repositories;

use App\Models\Profiles;

class ProfilesRepository
{
    protected $model;

    public function __construct(Profiles $model)
    {
        $this->model = $model;
    }
}