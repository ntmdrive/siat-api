<?php

namespace App\Repositories;

use App\Models\Permissions;

class PermissionsRepository
{
    protected $model;

    public function __construct(Permissions $model)
    {
        $this->model = $model;
    }
}