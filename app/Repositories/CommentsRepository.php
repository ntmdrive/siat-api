<?php

namespace App\Repositories;

use App\Models\Comments;

class CommentsRepository
{
    protected $model;

    public function __construct(Comments $model)
    {
        $this->model = $model;
    }
}