<?php

namespace App\Repositories;

use App\Models\Services;

class ServicesRepository
{
    protected $model;

    public function __construct(Services $model)
    {
        $this->model = $model;
    }
}