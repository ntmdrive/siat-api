<?php

namespace App\Repositories;

use App\Models\UsersProfiles;

class UsersProfilesRepository
{
    protected $model;

    public function __construct(UsersProfiles $model)
    {
        $this->model = $model;
    }
}