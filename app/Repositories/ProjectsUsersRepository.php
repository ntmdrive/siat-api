<?php

namespace App\Repositories;

use App\Models\ProjectsUsers;

class ProjectsUsersRepository
{
    protected $model;

    public function __construct(ProjectsUsers $model)
    {
        $this->model = $model;
    }
}