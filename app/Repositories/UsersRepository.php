<?php

namespace App\Repositories;

use App\Models\Users;

class UsersRepository
{
    protected $model;

    public function __construct(Users $model)
    {
        $this->model = $model;
    }
}