<?php

namespace App\Repositories;

use App\Models\Rules;

class RulesRepository
{
    protected $model;

    public function __construct(Rules $model)
    {
        $this->model = $model;
    }
}