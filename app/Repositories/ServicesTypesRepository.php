<?php

namespace App\Repositories;

use App\Models\ServicesTypes;

class ServicesTypesRepository
{
    protected $model;

    public function __construct(ServicesTypes $model)
    {
        $this->model = $model;
    }
}