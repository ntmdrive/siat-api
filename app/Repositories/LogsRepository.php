<?php

namespace App\Repositories;

use App\Models\Logs;

class LogsRepository
{
    protected $model;

    public function __construct(Logs $model)
    {
        $this->model = $model;
    }
}