<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    protected $guarded = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
